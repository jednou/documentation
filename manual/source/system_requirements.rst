===========================
Minimum System Requirements
===========================

OpenLP should run well on any fairly recent machine that can run a Chromium
browser. If you do not find specific installation instructions for your system,
you might still be able to run OpenLP but issues caused by your platform will
not be addressed. The most constraining OpenLP dependency is PyQt5-WebEngine.
If it is available for your system, chances are that you can run OpenLP from
`PyPI`_.

The following are suggested
specifications to get reasonable performance. It may be possible to run OpenLP
on less powerful hardware but you may not get the performance you desire.

* 2Ghz CPU
* 2GiB RAM
* 100MB free disk space
* Windows 10/11, Linux, macOS (10.8 or later)
* Multiple Monitor Support is highly recommended (not required)
* For Presentation support:

  * Windows: PowerPoint XP or later or LibreOffice Impress 4 or later
  * Linux: LibreOffice Impress 4 or later
  * macOS: Not supported yet due to limitations in Keynote and PowerPoint
* The Generic Document/Presentation and Songs of Fellowship song import options
  require LibreOffice 4 or later

:: _PyPI: https://pypi.org/project/OpenLP/
